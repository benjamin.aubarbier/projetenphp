<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="shortcut icon" type="image/png" href="/favicon.png" />
    <link rel="stylesheet" href="./reset.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <title>Contact</title>
</head>
<?php

// Appel du tableau $_SESSION

session_start();

// Contrôle d'accès

if (!isset($_SESSION["admin"]))
{
	header("location:admin.php?pirate");
}

// Message de bienvenue

echo('<div class="bienvenue">Bienvenue !</div>');

// Lien de déconnexion

echo("<p><a href=\"admin.php?logout\">Se déconnecter</a></p>");

?>

<div>
    <a>Ajouter un article</a>
    <form action="adminajout.php" method="post">
        <div>
            <label for="nom">Nom </label>
            <br>
            <input class="c3" type="text" id="nom" name="annonce_libelle" required>
        </div>
        <div>
            <label for="prix">Prix </label>
            <br>
            <input class="c3" type="text" id="prix" name="annonce_prix" required>
        </div>
        <div>
            <label for="image">Url image </label>
            <br>
            <input class="c3" type="text" id="url" name="annonce_image" required>
        </div>
        <div>
            <label for="description">Description </label>
            <br>
            <textarea class="c3" id="description" name="annonce_description" required></textarea>
        </div>
        <div>
            <label for="image">Genre article </label>
            <br>
            <input class="c3" type="text" id="genre" name="annonce_genre" placeholder="Meubles,Objets,Vêtements" required>
        </div>
        <div>
            <button class="c3" type="submit">Ajout !</button>
        </div>
    </form>
    <a>Supprimer un article</a>
    <form action="adminrejet.php" method="post">
        <div>
            <label for="nom">Nom </label>
            <br>
            <input class="c3" type="text" id="nom" name="annonce_libelle2" required>
        </div>
        <div>
            <button class="c3" type="submit">Ajout !</button>
        </div>
    </form>
</div>