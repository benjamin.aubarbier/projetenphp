<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="shortcut icon" type="image/png" href="/favicon.png" />
    <link rel="stylesheet" href="./reset.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <title>Contact</title>
</head>
<?php

// Appel du tableau $_SESSION

session_start();
$host = "localhost";
$user = "baubarbier";
$password = "biententé";
$bdd = "baubarbier";


$lien = mysqli_connect($host,$user,$password,$bdd);

// Cas d'un utilisateur ayant voulu afficher une page privée sans connexion

if (isset($_GET["pirate"]))
{
	echo("<p>Bien essayé mais il faut se connecter... :-)</p>");
}

// Déconnexion

if (isset($_GET["logout"]))
{
	unset($_SESSION["admin"]);
	
	// On peut également utiliser session_destroy()
	
	echo("<p>Vous avez bien été déconnecté</p>");
}

// Traitement

if (!empty($_POST))
{
	// Récupération des données de formulaires
	
	$mail = $_POST["mail"];
	$mdp = $_POST["password"];
	
	// Limite de l'exercice : un seul compte est valable
	
    $sql = "select * from admin where admin_mail = '".$mail."' and admin_mdp= '".$mdp."'";

    $result = mysqli_query($lien, $sql);
    
    $num = mysqli_num_rows($result);
    
    if ($num == 1){
        $_SESSION["admin"] = $mail;
        header('Location: adminconfirmation.php');
        
    }else{
        echo("<p>Erreur d'authentification !</p>");
    }
}

// Formulaire HTML en méthode POST qui renvoie vers la même page

?>
<div class="espace">Espace Connexion</p>
<form method="post" action="admin.php">
<p>E-mail <input type="email" name="mail" required /></p>
<p>Mot de passe <input type="password" name="password" required /></p>
<p><input type="submit" value="Se connecter" /></p>
</form>