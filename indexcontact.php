<!doctype html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="shortcut icon" type="image/png" href="/favicon.png" />
    <link rel="stylesheet" href="./reset.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <title>Contact</title>
</head>

<body class="body">
    <header class="header">
        <img src="./images/logo_capharnum.png" alt="" class="logo">
        <div class="barderecher">
            <form>
                <nav class="find">
                    <svg class="icon-find" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52.966 52.966"
                        style="enable-background:new 0 0 52.966 52.966;" xml:space="preserve">
                        <path d="M51.704,51.273L36.845,35.82c3.79-3.801,6.138-9.041,6.138-14.82c0-11.58-9.42-21-21-21s-21,9.42-21,21s9.42,21,21,21
                    c5.083,0,9.748-1.817,13.384-4.832l14.895,15.491c0.196,0.205,0.458,0.307,0.721,0.307c0.25,0,0.499-0.093,0.693-0.279
                    C52.074,52.304,52.086,51.671,51.704,51.273z M21.983,40c-10.477,0-19-8.523-19-19s8.523-19,19-19s19,8.523,19,19
                    S32.459,40,21.983,40z" />
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                    </svg>
                    <input class="inputsearch" type="search" name="search" id="search" placeholder="Recherche">
                </nav>
            </form>
        </div>
        <img src="./images/reseaux.png" class="reseau">
    </header>
    <div class="big--container">
        <div class="mini--container">
            <div class="big--container__content">
            <a class="article" href="http://baubarbier.eemi.tech/indexmeubles.php">Meubles</a>
                <a class="article" href="http://baubarbier.eemi.tech/indexvetements.php">Vêtements</a>
                <a class="article" href="http://baubarbier.eemi.tech/indexobjets.php">Objets</a>
            </div>
            <div href="#">
                <img src="./images/plus.png" class="plus">
                <a class="article--annonce" href="#">dépôt d'une annonce</a>
            </div>
        </div>
    </div>
    <div class="depot">
        <a>Dépôt d'une annonce</a>
    </div>
    <div class="contact">
        <div class="contact2">
            <form action="indexcontact2.php" method="post">
                <div class="prenom">
                    <div class="er">
                        <label for="name">Nom*</label>
                        <br>
                        <input class="c2" type="text" id="name" name="contact_nom" required>
                    </div>
                    <br>
                    <br>
                    <div class="er2">
                        <label for="prenom">Prénom*</label>
                        <br>
                        <input class="c2" type="text" id="prenom" name="contact_prenom" required>
                    </div>
                </div>
                <br>
                <br>
                <div class="er">
                    <label for="mail">E-mail*</label>
                    <br>
                    <input class="c2" type="email" id="mail" name="contact_email" required>
                </div>
                <br>
                <br>
                <div class="er">
                    <label for="telephone">Téléphone</label>
                    <br>
                    <input class="c2" type="tel" id="telephone" name="contact_telephone">
                </div>
                <br>
                <br>
                <div class="er">
                    <label for="message">Message*</label>
                    <br>
                    <textarea class="c2" id="message" name="contact_message" required></textarea>
                </div>
                <br>
                <br>
                <div class="er">
                    <label for="fichier">Lien image</label>
                    <br>
                    <input class="c2" type="text" name="contact_nomfichier" id="fichier">
                </div>
                <br>
                <div class="envoyer">
                    <button class="c2" type="submit">Envoyer</button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>